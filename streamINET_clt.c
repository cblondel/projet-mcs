#include "lib/session.h"

void	dialogueSrv(int sd, struct sockaddr_in *svc);
void	dialogueSrv2(int sd, struct sockaddr_in *svc);
int		traiterEntreeSocket(int sd, struct sockaddr_in *svc);
void 	traiterEntreeClavier(int sd);

int 	main(int ac, char **av)
{
	int 				s;
	struct sockaddr_in	svc;

	s = connectStream(s, &svc, "127.0.0.1");


	#ifdef TEST
	dialogueSrv(s, &svc);
	#else
	dialogueSrv2(s, &svc);
	#endif
	close(s);
	return (0);
}

void	dialogueSrv(int sd, struct sockaddr_in *svc)
{
	char	reponse[BUFF_MAX];

	// Envoi du message MSG au serveur : la réponse sera OK
	CHECK(write(sd, MSG, strlen(MSG) + 1), "Erreur write")
	CHECK(read(sd, reponse, sizeof(reponse)), "Erreur read")
	printf("Message reçu [%s] de [%s]\n", reponse, inet_ntoa(svc->sin_addr));

	// Envoi du message ERR au serveur : la réponse sera NOK
	CHECK(write(sd, ERR, strlen(ERR) + 1), "Erreur write")
	CHECK(read(sd, reponse, sizeof(reponse)), "Erreur read")
	printf("Message reçu [%s] de [%s]\n", reponse, inet_ntoa(svc->sin_addr));

	// Envoi du message BYE au serveur : la réponse sera la fin du dialogue
	CHECK(write(sd, BYE, strlen(BYE) + 1), "Erreur write")
	CHECK(read(sd, reponse, sizeof(reponse)), "Erreur read")
	printf("Message reçu [%s] de [%s]\n", reponse, inet_ntoa(svc->sin_addr));
}

void	dialogueSrv2(int sd, struct sockaddr_in *svc)
{
	int 	nb;
	fd_set	rfds;

	while (1)
	{
		FD_ZERO(&rfds);
		FD_SET(0, &rfds);
		FD_SET(sd, &rfds);
		CHECK(nb = select(sd + 1, &rfds, NULL, NULL, NULL), "Erreur select");
		if (nb == 0) continue;
		if (FD_ISSET(0, &rfds))
		{
			traiterEntreeClavier(sd);
			if (--nb == 0) continue;
		}
		if (FD_ISSET(sd, &rfds))
		{
			if (traiterEntreeSocket(sd, svc) == 0) break;
			if (--nb == 0) continue;
		}
	}
}

int		traiterEntreeSocket(int sd, struct sockaddr_in *svc)
{
	int		nbCarac;
	char	reponse[BUFF_MAX];

	CHECK(nbCarac = read(sd, reponse, sizeof(reponse)), "Erreur read socket");
	if (nbCarac == 0) return (0);
	printf("Message reçu [%s] de [%s]\n", reponse, inet_ntoa(svc->sin_addr));
	return (nbCarac);
}

void 	traiterEntreeClavier(int sd)
{
	int		nbCarac;
	char	message[BUFF_MAX];

	CHECK(nbCarac = read(0, message, sizeof(message)), "Erreur read clavier");
	CHECK(write(sd, message, strlen(message) + 1), "Erreur write");
}
