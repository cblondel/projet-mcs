#include "lib/session.h"

void	dialogueClt(int sd, struct sockaddr_in clt)
{
	char	requete[BUFF_MAX];

	do
	{
		read(sd, buffer, sizeof(buffer));
		//printf("buffer = %s\n", buffer);
		sscanf(buffer, "%3[^:]:%[^\n]", requete, buffer);
		//printf("\trequete = %s\n", requete);
		//printf("\tbuffer = %s\n", buffer);
		switch (atoi(requete))
		{
			case 0:
			{
				printf("Au revoir\n");
				break;
			}
			case 100:
			{
				write(sd, OK, strlen(OK) + 1);
				printf("OK : Message recu : [%s] de [%s]\n", buffer, inet_ntoa(clt.sin_addr));
				break;
			}
			default:
			{
				write(sd, NOK, strlen(NOK) + 1);
				printf("NOK : Message recu : [%s] de [%s]\n", buffer, inet_ntoa(clt.sin_addr));
				break;
			}
		}
	}
	while (atoi(requete) != 0);
}

int 	main()
{
	int 				se;
	int 				sd;
	socklen_t			cltLen;
	struct sockaddr_in 	svc;
	struct sockaddr_in 	clt;

	se = connectStream(se, &svc, "127.0.0.1");
	se = listenServer(se, &svc);

	printf("A l'ecoute\n");
	while(1)
	{
		// Attente d'un appel
		cltLen = sizeof(clt);
		CHECK(sd = accept(se, (struct sockaddr*)&clt, &cltLen), "Erreur accept")
		/*CHECK(pid = fork(), "Erreur fork")
		if (pid == 0)
		{
			close(se);*/

			// Dialogue avec le client
			dialogueClt(sd, clt);
			close(sd);
			/*exit(sd);
		}
		close(sd);*/
	}
	close(se);
	return (0);
}
