// includes
#include <unistd.h>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <signal.h>
#include <sys/wait.h>
#include <sys/select.h>
#include <sys/time.h>

// macro fonctions
#define CHECK(sts,msg) if ((sts) == -1) {perror(msg); exit(-1);}

// constantes
#define PORT_SVC 5000	//Port du serveur
#define INADDR_SVC "172.0.0.1"	//Adresse locale
#define BYE "000:Au revoir a tous"
#define ERR "200:Erreur a tous"
#define OK "OK"
#define NOK "NOT OK"
#define BUFF_MAX 1024
#define MSG "coucou"

// variables globales
char	buffer[BUFF_MAX];


void	deroute(int signal)
{
	int		status;
	pid_t	fils;

	switch (signal)
	{
		case SIGCHLD:
		{
			CHECK(fils = wait(&status), "Erreur wait")
			printf("%i : %i\n", fils, status);
			break;
		}
		case SIGUSR1:
		{
			printf("Fin Pause\n");
			break;
		}
	}
}

/**
*	Paramètre :
*	Fonction :
* Return :
*/
int connectStream(int s, struct sockaddr_in *svc, char* ip) {
	CHECK(s = socket(PF_INET, SOCK_STREAM, 0), "Erreur socket")
	printf("Creation socket num %2i\n", s);

	svc->sin_family = PF_INET;
	svc->sin_port = htons(PORT_SVC);
	if (ip == NULL)
	{
		svc->sin_addr.s_addr = inet_addr(ip);
		memset(&svc->sin_zero, 0, 8);
		CHECK(connect(s, (struct sockaddr*)svc, sizeof(*svc)), "Erreur connect")
	}	else {
		svc->sin_addr.s_addr = INADDR_ANY;
		memset(&svc->sin_zero, 0, 8);
		CHECK(bind(s, (struct sockaddr*)svc, sizeof(*svc)), "Erreur bind")
	}
	return s;
}

/**
*	Paramètre :
*	Fonction :
* Return :
*/
int listenServer(int se, struct sockaddr_in *svc) {
	CHECK(connect(se, (struct sockaddr*)svc, sizeof(*svc)), "Erreur connect")
	return se;
}
